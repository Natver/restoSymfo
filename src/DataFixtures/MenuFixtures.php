<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Menus;

class MenuFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $menu = new Menus();
            $menu->setNomPlat("Menu n°$i")
                 ->setDescription("Description du menu n°$i")
                 ->setImage("http://placehold.it/300x150")
                 ->setIngredients("non")
                 ->setPrix("50");
                 
            $manager->persist($menu);
        }

        $manager->flush();
    }
}
