<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Menus;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RestoController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('resto/index.html.twig', [
            'controller_name' => 'RestoController',
        ]);
    }

    /**
     * @Route("/menus", name="menus")
     */
    public function menus()
    {
        $repo = $this->getDoctrine()->getRepository(Menus::class);
        $menus = $repo->findAll('Titre de l\'article');
        return $this->render('resto/menus.html.twig', [
            'controller_name' => 'RestoController',
            'menus' => $menus
        ]);
    }

    /**
     * @Route("/menus/new", name="menu_new")
     * @Route("/menus/{id}/edit", name="menu_edit")
     */
    public function new(Menus $menu = null, Request $request, EntityManagerInterface $manager)
    {
        if(!$menu){
            $menu = new Menus();
        }

        $form = $this->createFormBuilder($menu)
            ->add('nomplat', TextType::class, [
                'attr' => [
                    'placeholder' => 'Nom plat'
                ]
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Description plat'
                ]
            ])
            ->add('ingredients', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Ingrédients plat'
                ]
            ])
            ->add('image', TextType::class, [
                'attr' => [
                    'placeholder' => 'Lien image plat'
                ]
            ])
            ->add('prix', TextType::class, [
                'attr' => [
                    'placeholder' => 'Prix plat'
                ]
            ])
            ->getForm();
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($menu);
            $manager->flush();

            return $this->redirectToRoute('menu_show', ['id' => $menu->getId()]);
        }
            return $this->render('resto/new.html.twig', [
            'formMenu' => $form->createView(),
            'editMode' => $menu->getId() !== null
        ]);
    }

    /**
     * @Route("/menus/{id}", name="menu_show")
     */
    public function show(Menus $menu)
    {
        return $this->render('resto/show.html.twig', [
            'menu' => $menu
        ]);
    }
    /**
     * @Route("/menus/{id}/reserv", name="menu_reserv")
     */
    public function reserv(Request $request, EntityManagerInterface $manager)
    {
        $client = new Client();

        $form = $this->createFormBuilder($client)
                     ->add('nom')
                     ->add('prenom')
                     ->add('telephone', TelType::class)
                     ->add('date', DateType::class)
                     ->add('reserv')

                     ->getForm();

        return $this->render('resto/reserv.html.twig', [
            'formClient' => $form->createView()
        ]);
    }
}
